\documentclass[11pt]{article}
\usepackage{graphicx}
\usepackage[hidelinks]{hyperref}
\graphicspath{ {images/} }
\usepackage{listings}
\usepackage{float}
\usepackage{xcolor}
\definecolor{greenCode}{rgb}{0, 0.5, 0}
\lstdefinestyle{codeStyle}{
	basicstyle={\small\ttfamily}, 
	numbersep=10pt,
	tabsize=2,
	showspaces=false,
	showstringspaces=false,
	comment=[l]{//},
	morecomment=[s]{/*}{*/},
	commentstyle=\color{greenCode},
	breaklines=true
}
\lstset{style=codeStyle}
\lstdefinelanguage{args}{
	sensitive=false,
	alsoletter={.},
	moredelim=[s][\color{red}]{<}{>},
	moredelim=[s][\color{blue}]{[}{]},
	moredelim=[is][\color{orange}]{:}{:},
	keywords=[0]{},
	keywordstyle=[10]{\color{magenta}},
}

\lstnewenvironment{arguments}
	{\lstset{language=args}}
	{}

\lstnewenvironment{bash}
	{\lstset{language=bash,keywordstyle={\color{blue}}}}
	{}
\title{Quickguide for the programs used in: Automatic Atlas-based Landmark Transfer for Ligaments Identification}
\author{Lukas Nocker}

\begin{document}
\begin{titlepage}
	\begin{center}
		\includegraphics[scale=0.6]{uibk_igs.png}\\
		\Large Leopold-Franzens-University Innsbruck \\
		\vspace*{0.5cm}
		Institute of Computer Science\\
		Interactive Graphics and Simulation Group\\
		\vspace*{2cm}
		\huge Quickguide for the programs used in:\\ Automatic Atlas-based Landmark Transfer for Ligaments identification\\
		\vspace*{3.5cm}
		\large Lukas Nocker\\
		\vspace*{3cm}
		\today
		\normalsize
	\end{center}
\end{titlepage}

%Index if required
%\pagenumbering{roman}
%\tableofcontents
%\newpage

\pagenumbering{arabic}
\section{Introduction}
	This document describes the installation and the usage of the programs that are created for the bachelor thesis 'Automatic Atlas-based Landmark Transfer for Ligaments Identification'. Here it is only described how the programs can be used and configured, for a description of the methods used and how it works have a look at the bachelor thesis itself.
	\par
	Note: The shown JSON files sometimes contain comments marked by \textcolor{greenCode}{//} and \textcolor{greenCode}{/* */} but they are just included to have a better understanding of the files, a real JSON file is not allowed to contain comments.

\section{Installation}
	The programs used in this project are based on Statismo, so it is required to install Statismo and its dependencies to compile the programs of this project. For the installation of Statismo have a look at \url{https://github.com/statismo/statismo}.
	\newline
	Note: It seems that the installation of Statismo has one of its dependencies missing, so it could lead to a missing 'proj\textunderscore lib.so' error message. To solve this problem, install proj\textunderscore lib by yourself before restarting the installation of Statismo.
	\par
	When Statismo and all its dependencies are installed this project can be compiled. For this download the project from \url{https://git.uibk.ac.at/csar8121/AutoLandmark}. Currently this project is accessible only for project members, but this can be extended to an internal or even public project in the future.
	\newline
	After downloading the project open its 'CMakeLists.txt' file and adjust the Statismo path to the location where you installed it. Now choose a folder where the project should be installed (typically just a build folder in the project itself), navigate into this folder and execute the following commands:
	\begin{bash}
		cmake <path to the 'AutoAtlas' folder>
		make -j 8
	\end{bash}
	Now the folder contains some files and folder created by cmake and the seven programs used in the project. The programs are now ready to use; how this can be done is explained in the next sections program by program.

\section{AtlasCreation}
	This is one of the two main programs of the project and it is used to generate atlases from a set of input data. Like all the programs it is a console program that takes a single argument. This argument is the path to a JSON file which contains all the configuration parameters and the input data. The output of the program is splitted in up to four different folders, as the program distinguishes bones of the left side from bones of the right side and also radii and ulnae will be treated independent. So for each type a own folder is created, which can be specified as parameters or as default './datadir/bonetype/side'. In this folders the aligned meshes will be stored as aligned\textunderscore ?.vtk file, where the ? is replaced with a up counting number; the Gaussian process model as 'gaussModel.h5'; the constrained models as 'constrained\textunderscore model\textunderscore ?.h5' where the ? matches up with the number of the aligned meshes; the meshes in correspondences as 'correspondent\textunderscore ?.vtk' where the ? again matches up with the number given to the aligned files; the atlas as 'statisticalModel.h5' and if created the enhanced atlas as 'enhancedAtlas.h5'.
	\begin{bash}
		./AtlasCreation <path to the configuration file>
	\end{bash}
	The configuration file for this program consists of two main parts; the part named 'parameters' contains all configuration parameters and the part named 'bones' that contains the input data.To distinguish radii from ulnae the bones part contains two sub parts, one for radii and one for ulnae.
	\lstinputlisting[caption={Base structure of the configuration file.},captionpos=b]{code/create_base_structure.json}
	The 'parameter' part offers the possibility to specify many different parameters of the program. For this program all the parameters are optional because for the required parameters default values are set and other parameters extend the functionality of the program but the main part works without them. Possible parameters are:
	\begin{description}
		\item "radius\textunderscore right\textunderscore folder", "radius\textunderscore left\textunderscore folder", "ulna\textunderscore right\textunderscore folder" and "ulna\textunderscore  \linebreak[0] left\textunderscore folder" specify the output folder for the generated data. If the input data set contains bones of a type the folder that is specified in the corresponding name is created and used as output folder. If the data set contains bones of a type where no output folder is specified, "datadir/bonetype/side" is used as the default folder.
		\item "rigid\textunderscore iter" specifies the maximal number of iterations for the rigid alignment, if not set the default value 1000 is used.
		\item "kernel\textunderscore sigma" specifies the sigma of the first level of the Gaussian process model. A lower sigma will result in a more flexible model but with less smoothness and shape stability. If not set 75.0 is used as default value.
		\item "kernel\textunderscore levels" specifies the number of levels that the multilevel Gaussian  process model should have. The levels allow to have a higher sigma for the base level for a smooth deformation but also allows to model small details. Too few levels with a high sigma result in loosing details and too many levels result in loosing the required smoothness. (default 3)
		\item "kernel\textunderscore scale" specifies the scaling factor that is applied on the Gaussian kernel for the Gaussian process model, so it defines the strength of possible deformations. (default 200)
		\item "gauss\textunderscore comp" specifies the number of primal components used to approximate the Gaussian process, a higher number will result in a better Gaussian process but also longer run time. If not set 250 is used as default. (For the data on which the program could be tested, values higher than 250 caused exceptions.)
		\item "gauss\textunderscore fit\textunderscore iter" specifies the maximal number of iterations that are done to fit the posterior model of the Gaussian process on the bones of the input data set. (default 1500)
		\item "correspond\textunderscore volume\textunderscore percentage" specifies the minimal percentage of volume that the fitted mesh has to have in comparison to the given mesh to be a valid. If not set 0.6 is used as default value. \linebreak[4] $V(fitted mesh) / V(input mesh) > "correspond\_volume\_percentage"$
		\item "landmark\textunderscore variance" specifies the variance of the landmark inaccuracy, a value greater zero is required for the calculations so if zero or less is specified 0.01 is used instead. (default 0.1)
		\item "atlas\textunderscore variance" allows the atlas to be a PPCA model with the here specified variance, by setting it to zero it will be a standard PCA model. (default 0)
		\item "atlas\textunderscore sigma" specifies the sigma of the Gaussian kernel used to enhance the atlas. If not set no enhanced atlas is created.
		\item "atlas\textunderscore scale" specifies the factor with which the Gaussian kernel for the enhanced atlas is multiplied. If not set no enhanced atlas is created.
		\item "atlas\textunderscore num\textunderscore comp" specifies the number of components that the combined model for the enhanced atlas should have. If not set no enhanced atlas is created.
		\item "gauss\textunderscore path\textunderscore radius\textunderscore r", "gauss\textunderscore path\textunderscore radius\textunderscore l", "gauss\textunderscore path\textunderscore ulna\textunderscore r" and \linebreak[4] "gauss\textunderscore path\textunderscore ulna\textunderscore l" if one ore more if this parameters are set, the step of generating a Gaussian process model for the bones of the group that corresponds to the name of the parameter is skipped and instead the given model is used to generate the posterior models. This allows to reuse a GP model from an earlier execution of this program.
	\end{description}
	\lstinputlisting[caption={Example parameters.},captionpos=b]{code/create_parameters.json}

	The part with the input data consists of two lists, one for radii and one for ulnae. For both lists such an entry contains three elements:
	\begin{description}
		\item "path" specifies the path to a .vtk or .stl file that is the mesh used as input.
		\item "side" has to be "r" or "l", where "r" means that this input is a bone of the right side and "l" means the left side.
		\item "landmarks" this entry contains a list of landmark coordinates. The name of each entry in this list is the landmark name and contains the three sub entries "x", "y" and "z" that specify the coordinates of the landmark.
	\end{description}
	\lstinputlisting[caption={Example of a bone entry.},captionpos=b]{code/create_bone_entry.json}
	\par
	Note: The ITK registration framework only uses one thread, so to increase the performance of the 'achieving correspondence' step, a thread for each bone is launched. Which heavily decreases the computation time but increases the memory usage. For more bones this could lead to memory problems depending on available amount, removing the parallelism could help there or even better a thread pool that keeps track of the available resources.


\section{FitAtlas}
	This is the second main program of the project, it is used to fit an atlas on a patient mesh and identify the ligaments on the bone. In addition this program performs error calculations if control landmarks are given in the configuration. Like the 'AtlasCreation' program it is a console program with one argument, which is the path to the configuration file.
	\begin{bash}
		./FitAtlas <path to the configuration file>
	\end{bash}
	This configuration file can contain the following parameters:
	\begin{description}
		\item "atlas\textunderscore path" the path to the atlas that should be used for this fitting, this parameter is required and the program will give an error message if the parameter is not set or the specified file cannot be found.
		\item "patient\textunderscore path" the path to the mesh on which the atlas should be fitted, this can be a .stl or .vtk file. This is a required parameter and the program will print an error message if the path is not set or the specified file does not exist.
		\item "type" has to be "radius" or "ulna". This parameter is required so that the program knows what landmarks are contained in the model. Without this parameter the program will exit with an error message.
		\item "align\textunderscore iter" specifies the maximal number of iterations during the alignment of the patient mesh to the atlas. If not set the default value of 1000 is used.
		\item "fit\textunderscore iter" specifies the maximal number of iterations for the fitting of the atlas to the patient mesh.  If not set the default value of 2000 is used.
		\item "vtk\textunderscore output\textunderscore path" deters the output path for the two generated meshes. If not set the current working directory is used as output folder.
		\item "json\textunderscore output\textunderscore path" specifies the path and file name of a JSON file that can be generated as additional output. If not set no JSON file is created as output.
		\item "scalismo\textunderscore gui\textunderscore path" specifies the path and file name of an additional output file. This file is a text file containing scala code that can be executed in the program Scalismo-ui to get a graphical visualization of the output. If not set the file will not be generated.
		\item "landmarks" this parameter allows to evaluate the fitting by performing the fitting on  a mesh where the landmarks are known, by passing the landmark coordinates to the program over this parameter error calculations are done and stored in the JSON file, if the path for the JSON file is set. The structure of this parameter is the same as the 'landmark' parameter in the configuration file for the atlas creation.
	\end{description}
	\lstinputlisting[caption={Example AtlasFit configuration.},captionpos=b]{code/fit.json}
	The main output of this program consists of two .vtk files, the first is called 'fit\textunderscore aligned.vtk' which is the input mesh but aligned with the model and the second file is called 'output.vtk' which is the same mesh as the other one but this time with additional points that represent the landmarks. Which landmark is located on which index depends on the bone type and the total number of points in the mesh and can be taken from the following table:
	\begin{table}[H]
		\begin{center}
			\begin{tabular}{ | l | r | r |}
				\hline
				\multicolumn{3}{ | c | }{Landmarks} \\
				\multicolumn{1}{ | c | }{Index} &
				\multicolumn{1}{ | c | }{Radius} &
				\multicolumn{1}{ | c | }{Ulna} \\
				\hline
				n-1 & PRUL\textunderscore R & PRULd\textunderscore U \\
				n-2 & DRUL\textunderscore R & PRULs\textunderscore U \\
				n-3 & DOAC\textunderscore R & DRULd\textunderscore U \\
				n-4 & POC\textunderscore R & DRULs\textunderscore U \\
				n-5 & DOB\textunderscore R & DOAC\textunderscore U \\
				n-6 & AB\textunderscore R & POC\textunderscore U \\
				n-7 & CBP\textunderscore R & DOB\textunderscore U \\
				n-8 & CBD\textunderscore R & AB\textunderscore U \\
				n-9 & RAF\textunderscore P & CBP\textunderscore U \\
				n-10 & & CBD\textunderscore U \\
				n-11 & & RAF\textunderscore D \\
				\hline
			\end{tabular}
			\caption{Indices of the landmarks depending on the bone type and $n$ := the number of points in the mesh.}
		\end{center}
		\label{tab:landmark_indices}
	\end{table}
	With the parameter "scalismo\textunderscore gui\textunderscore path" set a text file is generated that contains scala code that can be loaded in the Scalismo-ui program or Scalismolab which can be downloaded from:
	\newline
	\url{https://github.com/unibas-gravis/scalismo/wiki/scalismoLab}.\newline This code displays the mesh and the landmarks on it, if control landmarks are given they are also displayed to have a graphical comparison between the identified landmarks and the real one.
	\par
	If the "json\textunderscore output\textunderscore path" is set a JSON file is created that contains three entries and three additional if control landmarks are given. This JSON file gives a good alternative to the output.vtk file as this file contains the landmarks as mesh points that are not part of the cell structure, so the mesh is no longer two manifold which can be required for some further steps. For this case and all other cases where the landmark coordinates should be accessible as values the JSON file can be accessed to get them, this coordinates correspond to the landmarks on the surface of the mesh stored in the 'fit\textunderscore aligned.vtk' file.
	\begin{description}
		\item "vtk\textunderscore path" the path to the 'output.vtk' file.
		\item "patient" the path to the mesh on which the atlas was fitted.
		\item "landmarks" contains the list off all landmarks and their coordinates in the generated meshes.
		\item "detailed\textunderscore errors", only generated when control landmarks are given, contains a list with all landmarks and a value that represents the euclidean distance from the identified landmark position to the control landmark.
		\item "error\textunderscore sum", only created when control landmarks are given, this value is the sum of all errors in the detailed errors section.
		\item "squared\textunderscore error\textunderscore sum", only created when control landmarks are given. This value is the sum of all squared errors and can be used as metric for a good fitting, for example when different parameters are compared.
	\end{description}
	\lstinputlisting[caption={Example JSON output.},captionpos=b]{code/outputExt.json}

\section{GenerateTests}
	This program can be used to generate test cases. The program takes one argument which is the path to a JSON file that has the same structure than the configuration file for the 'AtlasCreation' program.
	\begin{bash}
		./GenerateTests <path to the configuration file>
	\end{bash}
	But instead of generating an atlas, this program generates many configuration files that represent all possible test cases for this data set. For this in the specified output folder a series of folders is generated. Each of this folders represents one possible test case, where one mesh is excluded from the input data set and then the fitting is performed on the excluded mesh to evaluate the accuracy of the process. All folders contain three files: the 'build.json' file, which is the configuration file for the 'AtlasCreation' program; the 'fit.json' file, which is the configuration file for the 'FitAtlas' program and a 'run.sh' file which is a simple shell script that calls the atlas creation program and after that the fitting program, so a test can be executed with a single call of the script. To run all tests an additional shell script called 'runAll.sh' is generated in the current working directory, this script executes the run scripts off all the test cases.

	\section{Statistics}
		This program can be used to generate statistics out of the JSON output files from the fitting. Important for the program is that the JSON files contain the error information, means that the atlas fitting has to be done with control landmarks.\newline
The program again takes a single argument, which is the path to a JSON configuration file.
		\begin{bash}
			./Statistics <path to the configuration file>
		\end{bash}
	The configuration file needs to contain a list called "paths", this list contains the path to all output files that should be included in the statistics. In addition to this parameter two file paths can be specified, the first parameter is "stat\textunderscore file\textunderscore path", which deters the file file where the statistics should be written, if this parameter is not set the output will be written in the console window. The second parameter is "r\textunderscore file\textunderscore path", if this parameter is set an additional file is generated that contains code written in R.
	\lstinputlisting[caption={Example configuration file.},captionpos=b]{code/stat_config.json}
	To calculate the statistics the program groups the input data in two different ways: by landmark name and by bone, means by input file. For each group then the minimum, maximum, mean, variance and standard derivation is calculated and written in the output file or console. If the "r\textunderscore file\textunderscore path" is set, a file is created that contains code written in R. This code can be executed to receive boxplot diagrams, which is an easy way to get a graphical representation of the statistics.


\section{ConfigCreate}
	A small console program that takes one parameter, which is the file name of the file that should be created from this program.
	\begin{bash}
		./ConfigCreate <file name of the output>
	\end{bash}
	The program will ask many user inputs and generates a JSON file out of the requested input that can be used as configuration file for the 'AtlasCreation' program. To generate a configuration file with this program takes a lot of time but allows users to create a configuration file even when they do not know how the JSON file is structured.

\section{ConfigFit}
	Like 'ConfigCreate' this program is also used to generate a configuration file from user inputs. The output file name is again specified as the program parameter.
	\begin{bash}
		./ConfigFit <file name of the output>
	\end{bash}
	The file generated from this program can be used as configuration file for the 'AtlasFit' program.

\section{Tune}
	This is a small program that runs the atlas creation and fitting process multiple times with different parameters to identify the parameter combination that works best for this specific case. To keep it simple only the three most significant parameters vary during the performed tests. The search of the best parameters is within a specified range with a defined step length. As the program performs an exhaustive search a really high amount of tests is done and the program has a very high run time, which makes the program unusable for a real parameter specification and should only gives an example how the parameters could be tested automatically.
	\newline
	Like all the other programs it is a console program that takes a single parameter which is the path of a configuration file in JSON format.
	\begin{bash}
		./Tune <path to the configuration file>
	\end{bash}
	The configuration file has to contain the following 13 entries:
	\begin{description}
		\item "build\textunderscore config\textunderscore path" the path to a configuration file for the 'AtlasCreation' program.
		\item "fit\textunderscore config\textunderscore path" the path to a configuration file for the 'FitAtlas' program, important for this procedure is that this file contains control landmarks to measure the landmark error for the tested parameters.
		\item "type" has to be "ulna" or "radius" to specify the bone type.
		\item "side" has to be "r" or "l", where "r" means that the bones are from the right side and "l" means that the bones are from the left side.
		\item "gp\textunderscore sigma\textunderscore min" specifies the lower limit of the search for the best sigma used in the Gaussian process model.
		\item "gp\textunderscore sigma\textunderscore max" specifies the upper limit of the search for the best sigma used in the Gaussian process model.
		\item "gp\textunderscore sigma\textunderscore step" specifies the step size of the search for the best sigma used in the Gaussian process model.
		\item "at\textunderscore sigma\textunderscore min" specifies the lower limit of the search for the best sigma used in the Gaussian kernel for the enhanced model.
		\item "at\textunderscore sigma\textunderscore max" specifies the upper limit of the search for the best sigma used in the Gaussian kernel for the enhanced model.
		\item "at\textunderscore sigma\textunderscore step" specifies the step size of the search for the best sigma used in the Gaussian kernel for the enhanced model.
		\item "levels\textunderscore min" specifies the lower limit of the search for the best number of levels for the multilevel Gaussian process model.
		\item "levels\textunderscore max" specifies the upper limit of the search for the best number of levels for the multilevel Gaussian process model.
		\item "levels\textunderscore step" specifies the step size of the search for the best number of levels for the multilevel Gaussian process model.
	\end{description}
	The output of the program is a folder called 'atlas\textunderscore tune' in the current working directory that contains many pairs of JSON files called 'build\textunderscore ?.json' and 'fit\textunderscore ?.json' where the ? are replaced with matching up counting numbers. This files are the generated configuration files with different parameters. For each pair of configuration files a folder called 'atlas\textunderscore tune?', where the ? is replaced with the corresponding number, is created in the output folder specified in the original given configuration file. This folders will then contain all files generated during the atlas generation and fitting.
	\lstinputlisting[caption={Example tune configuration.},captionpos=b]{code/tune_config.json}
\end{document}
