file_c1 = new File("D:/Bachelor/AutoAtlas/paper/images/generation_resources/ac_not_cor_def/aligned_1.vtk")
val mesh_c1 = MeshIO.readMesh(file_c1).get
show(mesh_c1, "mesh_c1")

val file_c2 = new File("D:/Bachelor/AutoAtlas/paper/images/generation_resources/ac_not_cor_def/aligned_0.vtk")
val mesh_c2 = MeshIO.readMesh(file_c2).get
show(mesh_c2, "mesh_c2")

val deformations1 : IndexedSeq[Vector[_3D]] = mesh_c1.pointIds.map{ 
  id : PointId =>  mesh_c2.point(id) - mesh_c1.point(id)
}.toIndexedSeq

val vecField1 = DiscreteVectorField(mesh_c1, deformations1)
show(vecField1, "deformations1")